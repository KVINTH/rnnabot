﻿using org.mariuszgromada.math.mxparser;
using RnnaBot.Data;
using RnnaBot.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types.Enums;
using YahooFinanceApi;
using Message = Telegram.Bot.Types.Message;

namespace RnnaBot
{
    class Program
    {
        private static TelegramBotClient botClient;
        private static readonly IQuoteRepository quoteRepository = new QuoteRepository();
        private static readonly IPhotoRepository photoRepository = new PhotoRepository();
        private static Random.Org.Random random;

        static void Main(string[] args)
        {
            InitializeApiKeys();
            InitializeEventHandlers();
            random = new Random.Org.Random();
            var me = botClient.GetMeAsync().Result;
            Console.Title = me.Username;
            botClient.StartReceiving(Array.Empty<UpdateType>());
            Console.WriteLine($"Start listening for @{me.Username}");

            var input = string.Empty;
            do
            {
                input = Console.ReadLine();
                if (input.Contains("*"))
                {
                    var inputs = input.Split('*');
                    var chatId = inputs[0];
                    var output = inputs[1];
                    botClient.SendTextMessageAsync(
                        chatId,
                        output);
                }

            } while (input != "quit");          
            botClient.StopReceiving();
        }

        #region Event Handlers
        private static async void BotOnMessageReceived(object sender, MessageEventArgs messageEventArgs)
        {
            var message = messageEventArgs.Message;
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("{0} - {2}({1}): {3}", message.Chat.Id, message.From.Id, message.From.Username, message.Text);
            Console.WriteLine(sb);
            switch (message.Type)
            {
                case MessageType.Text:
                    switch (message.Text.Split(' ').First())
                    {
     
                        case "":
                            break;
                        case "/photo":
                            await SendPhoto(message);
                            break;
                        case "/quote":
                            await SendQuote(message);
                            break;
                        case "/addquote":
                            await AddQuote(message);
                            break;
                        case "/cash":
                            await ConvertCash(message);
                            break;
                        case "/calc":
                            await Calculate(message);
                            break;
                        case "/kick":
                            await KickUser(message);
                            break;
                        case "/roll":
                            await Roll(message);
                            break;
                        //case "/crypto":
                        //    await ConvertCrypto(message);
                        //    break;
                        case "/dice":
                            await Dice(message);
                            break;
                                
                    }
                    break;
                case MessageType.Photo:
                    if (message.Text == null && message.Caption == null) return;
                    switch (message.Caption.Split(' ').First())
                    {
                        case "/addphoto":
                            await AddPhoto(message);
                            break;
                    }
                    break;
                default:
                    return;
            }
            

        }
        #endregion

        #region Action Methods
        private static async Task SendPhoto(Message message)
        {
            await botClient.SendChatActionAsync(message.Chat.Id, ChatAction.UploadPhoto);
            using (var context = new RnnABotContext())
            {
                var random = new Random.Org.Random();
                var photos = photoRepository.GetPhotos().ToList();
                var photo = photos.ElementAt(random.Next(0, photos.Count()));
                var file = await botClient.GetFileAsync(photo.TelegramFileId);

                await botClient.SendPhotoAsync(
                    message.Chat.Id,
                    file.FileId,
                    "");
            }        
        }
        private static async Task AddPhoto(Message message)
        {
            var photos = new List<Photo>();
            foreach (var photo in message.Photo)
            {
                var dbPhoto = new Photo
                {
                    TelegramFileId = photo.FileId
                };
                photos.Add(dbPhoto);
                
            }
            bool success = await photoRepository.AddMultiple(photos);
            await botClient.SendTextMessageAsync(
                message.Chat.Id,
                success ? "Photo Saved Successfully" : "Photo Failed to Save");

        }
        private static async Task SendQuote(Message message)
        {
            await botClient.SendChatActionAsync(message.Chat.Id, ChatAction.Typing);
            var random = new Random.Org.Random();
            var quotes = quoteRepository.GetQuotes().ToList();
            var quote = quotes.ElementAt(random.Next(0, quotes.Count()));

            await botClient.SendTextMessageAsync(
                message.Chat.Id,
                quote.Text);

        }
        private static async Task AddQuote(Message message)
        {
            var quote = message.Text.Replace("/addquote ", "");
            
            if (quote != null || quote != "")
            {
                await botClient.SendChatActionAsync(message.Chat.Id, ChatAction.Typing);               

                var dbQuote = new Quote
                {
                    Text = quote
                };                
                bool success = await quoteRepository.Add(dbQuote);
                await botClient.SendTextMessageAsync(
                    message.Chat.Id,
                    success ? "Quote Saved Successfully" : "Quote Failed to Save");
       
            }
        }
        private static async Task ConvertCash(Message message)
        {
            await botClient.SendChatActionAsync(message.Chat.Id, ChatAction.Typing);
            var receivedMessage = message.Text.Replace("/cash", "").Trim();
            var messageParts = receivedMessage.Split(' ');

            //if (await TrollGurvinder(message) == true) return;
            if (messageParts.Length == 4)
            {
                var valueToConvert = messageParts[0];
                var fromCurrency = messageParts[1];
                var toCurrency = messageParts[3];
                var convertedValue = FixerSharp.Fixer.ConvertAsync(fromCurrency, toCurrency, Double.Parse(valueToConvert));
                var sb = new StringBuilder();

                if (!convertedValue.IsFaulted)
                {
                    sb.AppendFormat("{0} {1} in {2} is {3}", valueToConvert, fromCurrency, toCurrency, convertedValue.Result.ToString("#.00"));
                }
                else
                {
                    sb.Append(convertedValue.Exception.InnerException.Message);
                }
                

                await botClient.SendTextMessageAsync(
                    message.Chat.Id,
                    sb.ToString());
            }
            else
            {
                await botClient.SendTextMessageAsync(
                    message.Chat.Id,
                    "Usage: /cash 100 USD to CAD");
            }

        }
        private static async Task Calculate(Message message)
        {          
            await botClient.SendChatActionAsync(message.Chat.Id, ChatAction.Typing);
            //if (await TrollGurvinder(message) == true) return;
            var receivedMessage = message.Text.Replace("/calc", "");
            Expression expression = new Expression(receivedMessage);
            await botClient.SendTextMessageAsync(
                message.Chat.Id,
                expression.calculate().ToString());
        }
        private static async Task KickUser(Message message)
        {
            var administrators = await botClient.GetChatAdministratorsAsync(message.Chat.Id);

            if (administrators.Any(x => x.User.Username == "RnnABot"))
            {
                var receivedMessage = message.Text.Replace("/kick", "");
                var entities = message.Entities;
                var selectedEntity = entities[1];
                var userToKick = selectedEntity.User?.Id;

                if (userToKick.HasValue)
                {
                    await botClient.KickChatMemberAsync(
                    message.Chat.Id,
                    userToKick.Value);
                }
            }
            
        }
        private static async Task Roll(Message message)
        {
            await botClient.SendChatActionAsync(message.Chat.Id, ChatAction.Typing);
            await botClient.SendTextMessageAsync(
                message.Chat.Id,
                random.Next(0, 100).ToString());
        }
        private static async Task ConvertCrypto(Message message)
        {
            var btc = await Yahoo.Symbols("BTC").Fields(Field.RegularMarketPrice).QueryAsync();
            var aaa = btc["BTC"];
            var price = aaa[Field.RegularMarketPrice];
        }
        private static async Task Dice(Message message)
        {
            await botClient.SendChatActionAsync(message.Chat.Id, ChatAction.Typing);
            
            var sb = new StringBuilder();
            sb.AppendFormat("Dice One rolled... {0}", random.Next(1, 6).ToString());
            sb.AppendLine();
            sb.AppendFormat("Dice Two rolled... {0}", random.Next(1, 6).ToString());
            var diceRoll =
            await botClient.SendTextMessageAsync(
                chatId: message.Chat.Id,
                text: sb.ToString(),
                replyToMessageId: message.MessageId);
        }
        #endregion

        #region Helper Methods
        private static void InitializeApiKeys()
        {
            var basePath = Path.GetDirectoryName(Path.GetDirectoryName(Path.GetDirectoryName(Directory.GetCurrentDirectory())));
            DotNetEnv.Env.Load(basePath + "/.env");

            botClient = new TelegramBotClient(Environment.GetEnvironmentVariable("TELEGRAM_API_KEY"));
            FixerSharp.Fixer.SetApiKey(Environment.GetEnvironmentVariable("FIXER_API_KEY"));
        }
        private static void InitializeEventHandlers()
        {
            botClient.OnMessage += BotOnMessageReceived;
            botClient.OnMessageEdited += BotOnMessageReceived;
        }
        private static async Task<bool> TrollGurvinder(Message message)
        {
            if (message.From.Username.ToLower().Contains("pivx"))
            {
                await botClient.SendTextMessageAsync(
                    message.Chat.Id,
                    "Gureedpak shoulda sold for 38k sats");
                return true;
            }
            return false;
        }
        #endregion
    }
}
