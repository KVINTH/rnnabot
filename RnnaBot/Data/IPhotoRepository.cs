﻿using RnnaBot.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RnnaBot.Data
{
    public interface IPhotoRepository
    {
        Task<bool> Add(Photo photo);
        Task<bool> AddMultiple(List<Photo> photos);
        Task<bool> Edit(Photo photo);
        Task<bool> Remove(int id);

        IEnumerable<Photo> GetPhotos();
        Photo FindById(int Id);
    }
}
