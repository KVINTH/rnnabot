﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RnnaBot.Model;

namespace RnnaBot.Data
{
    public class PhotoRepository : IPhotoRepository
    {
        RnnABotContext rnnaContext = new RnnABotContext();

        public async Task<bool> Add(Photo photo)
        {
            rnnaContext.Photos.Add(photo);
            return await rnnaContext.SaveChangesAsync() > 0;
        }

        public async Task<bool> Edit(Photo photo)
        {
            rnnaContext.Entry(photo).State = System.Data.Entity.EntityState.Modified;
            return await rnnaContext.SaveChangesAsync() > 0;
        }
        public async Task<bool> Remove(int id)
        {
            var photo = rnnaContext.Photos.Find(id);
            rnnaContext.Photos.Remove(photo);
            return await rnnaContext.SaveChangesAsync() > 0;
        }
        public Photo FindById(int Id)
        {
            return rnnaContext.Photos.Find(Id);
        }

        public IEnumerable<Photo> GetPhotos()
        {
            return rnnaContext.Photos;
        }

        public async Task<bool> AddMultiple(List<Photo> photos)
        {
            rnnaContext.Photos.AddRange(photos);
            return await rnnaContext.SaveChangesAsync() > 0;
        }
    }
}
