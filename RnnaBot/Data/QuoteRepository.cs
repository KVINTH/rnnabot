﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RnnaBot.Model;

namespace RnnaBot.Data
{
    public class QuoteRepository : IQuoteRepository
    {
        RnnABotContext rnnaContext = new RnnABotContext();

        public async Task<bool> Add(Quote quote)
        {
            rnnaContext.Quotes.Add(quote);
            return await rnnaContext.SaveChangesAsync() > 0;
        }

        public async Task<bool> Edit(Quote quote)
        {
            rnnaContext.Entry(quote).State = System.Data.Entity.EntityState.Modified;
            return await rnnaContext.SaveChangesAsync() > 0;
        }
        public async Task<bool> Remove(int id)
        {
            var quote = rnnaContext.Quotes.Find(id);
            rnnaContext.Quotes.Remove(quote);
            return await rnnaContext.SaveChangesAsync() > 0;
        }
        public Quote FindById(int Id)
        {
            return rnnaContext.Quotes.Find(Id);
        }

        public IEnumerable<Quote> GetQuotes()
        {
            return rnnaContext.Quotes;
        }


    }
}
