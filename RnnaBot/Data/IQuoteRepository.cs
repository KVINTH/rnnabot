﻿using RnnaBot.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RnnaBot.Data
{
    public interface IQuoteRepository
    {
        Task<bool> Add(Quote quote);
        Task<bool> Edit(Quote quote);
        Task<bool> Remove(int id);

        IEnumerable<Quote> GetQuotes();
        Quote FindById(int Id);
    }
}
