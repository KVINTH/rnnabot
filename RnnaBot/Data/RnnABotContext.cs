﻿using RnnaBot.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RnnaBot.Data
{
    public class RnnABotContext : DbContext
    {
        public RnnABotContext() : base()
        {

        }

        public DbSet<Quote> Quotes { get; set; }
        public DbSet<Photo> Photos { get; set; }
    }
}
