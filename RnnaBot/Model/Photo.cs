﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RnnaBot.Model
{
    public class Photo
    {
        public int Id { get; set; }
        public byte[] Content { get; set; }
        public string TelegramFileId { get; set; }
    }
}
